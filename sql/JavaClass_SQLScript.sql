create database if not exists employeemgmt;

use employeemgmt;

drop table if exists Employee;
create table Employee (id int NOT NULL AUTO_INCREMENT, 
						firstname varchar(25), lastname varchar(25), 
						ssn varchar(9), dob varchar(10), salary double, 
                        position varchar(25), 
				PRIMARY KEY (ID));
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('David', 'Goude', '112233121', '12-13-2018', 1234.00, 'Chairman');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Carla', 'Smith', '121333121', '10-13-1998', 200000.00, 'IT Analyst');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Joe', 'Young', '112233222', '12-13-1985', 123400.00, 'Architect');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Katie', 'Clark', '112000121', '12-13-2000', 12340.00, 'Intern');

select * from employee;
drop table Employee;

show databases;