package com.techosmium.bo;

import java.util.List;

import com.techosmium.model.Employee;

public interface EmployeeBO {

	public List<Employee> getAllEmployee();

}
