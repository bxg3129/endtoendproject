package com.techosmium.bo;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.techosmium.dao.EmployeeDAO;
import com.techosmium.model.Employee;

@Service
public class EmployeeBOImpl implements EmployeeBO {
	@Autowired
	EmployeeDAO employeedao;

	public EmployeeBOImpl() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public List<Employee> getAllEmployee() {
		// TODO Auto-generated method stub
		return employeedao.getAllEmployee();
	}

}
