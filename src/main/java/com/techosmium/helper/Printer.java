package com.techosmium.helper;

import java.util.List;

import com.techosmium.model.Employee;

public class Printer {

	private void printHeader() {
		System.out.println(String.format("|%-15s|%-15s|%-15s|%-15s|%-15s|", "First Name", "Last Name", "DOB",
																			"Profession", "Salary"));
	}

	public void print(Employee[] employees) {
		printHeader(); // prints Header Info
		// Access employees one by one
		for (int i = 0; i < employees.length; i++) {
			printEmployeeInfo(employees[i]);
		}
		printFooter();
	}

	public void print(List<Employee> emplList) {
		printHeader();

		//Advance For loop
		for (Employee e : emplList) {
			printEmployeeInfo(e);
		}

		printFooter();
	}

	private void printFooter() {
		// TODO print footer part

	}

	private void printEmployeeInfo(Employee emp) {
		// TODO Auto-generated method stub
		System.out.println(String.format("|%-15s|%-15s|%-15s|%-15s|%-15.2f|", emp.getFirstName(), emp.getLastName(),
				emp.getDateOfBirth(), emp.getProfession(), emp.getSalary()));
	}

	private class innerClass {
		String someString = "blah";
	}

}
