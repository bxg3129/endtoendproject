package com.techosmium.dao;

import java.util.List;

import com.techosmium.model.Employee;

public interface EmployeeDAO {
	public List<Employee> getAllEmployee();
}
