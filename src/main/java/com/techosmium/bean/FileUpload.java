package com.techosmium.bean;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.servlet.http.Part;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.techosmium.bo.EmployeeBO;
import com.techosmium.helper.FileParser;
import com.techosmium.model.Employee;

@Component
@ManagedBean(name = "fileUpload")
@RequestScoped
public class FileUpload {
	private Part file;
	private String fileContent;
	
	private List<Employee> empList;
	
	@Autowired
	FileParser fileParser;
	
	@Autowired
	EmployeeBO employeeBO;

	public FileUpload() {
		// TODO Auto-generated constructor stub
	}

	public void upload() {
		try {
			
			fileContent = new Scanner(file.getInputStream()).useDelimiter("\\A").next();

			empList = fileParser.parseEmployees(fileContent);
			
			for(Employee e: empList){
				System.out.println(e);
			}
			
		} catch (IOException e) {
			// Error handling
		}
	}

	public void validateFile(FacesContext ctx, UIComponent comp, Object value) {
		List<FacesMessage> msgs = new ArrayList<FacesMessage>();
		Part file = (Part) value;
		if (file.getSize() > 1024) {
			msgs.add(new FacesMessage("file too big"));
		}
		if (!"text/plain".equals(file.getContentType())) {
			msgs.add(new FacesMessage("not a text file"));
		}
		if (!msgs.isEmpty()) {
			throw new ValidatorException(msgs);
		}
	}

	public Part getFile() {
		return file;
	}

	public void setFile(Part file) {
		this.file = file;
	}
	
	 public String getFileContent() {
		return fileContent;
	}

	public List<Employee> getEmpList() {
		return employeeBO.getAllEmployee();
	}

	public void setEmpList(List<Employee> empList) {
		this.empList = empList;
	}
}
