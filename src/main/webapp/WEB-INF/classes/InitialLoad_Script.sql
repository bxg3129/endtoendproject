--initial data load
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('David', 'Goude', '112233121', '12-13-2018', 1234.00, 'Chairman');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Carla', 'Smith', '121333121', '10-13-1998', 200000.00, 'IT Analyst');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Joe', 'Young', '112233222', '12-13-1985', 123400.00, 'Architect');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Katie', 'Clark', '112000121', '12-13-2000', 12340.00, 'Intern');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Kim', 'K', '112100121', '12-13-1980', 12340.00, 'Supervisor');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Randy', 'Amith', '112000000', '12-13-1990', 12340.00, 'Intern');
insert into employee (firstname, lastname, ssn, dob, salary, position) values ('Joe', 'Sharp', '132000121', '12-13-1962', 124540.00, 'Boss');